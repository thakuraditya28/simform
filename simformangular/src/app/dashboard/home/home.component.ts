import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SocialAuthService } from 'angularx-social-login';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private router: Router,private socialAuthService: SocialAuthService) { }
  userFullName:string = localStorage.getItem('userFullName')
  ngOnInit(): void {
  }
  logout(){
    localStorage.removeItem('accessToken');
    localStorage.removeItem('userFullName');
    this.socialAuthService.signOut().then(() => {
      this.router.navigate(['/login']);
    });
  }
}
