import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from 'src/app/user.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  categoryForm:FormGroup;
  isLoggedin: boolean; 
  categories:any;
  constructor(private userservice: UserService,private fb:FormBuilder) {
    this.categoryForm = this.fb.group({
      name:['',[Validators.required]],
      description:['',[Validators.minLength(6)]],
      image:[],
    })
   }

  ngOnInit(): void {
    this.getCategory();

  }
  certEscolar:any;
  onFileChanged(event: any) {
    if (event.target.files && event.target.files.length) {
    const file = event.target.files[0];
    this.certEscolar = file;
    }
  }
  searchCategory(keyword){
    this.userservice.searchCategory({keyword}).subscribe((category:any) => {
      if(category.httpStatus == 200 && category.status == 'OK'){
        this.categories = category.data;
      }
    })
  }
  addCategory(event){
    let myFormValue = this.categoryForm.value

    const myFormData = new FormData()
    myFormData.append('image',this.certEscolar,this.certEscolar.name)
    myFormData.append('name',myFormValue.name)
    myFormData.append('description',myFormValue.description)
    this.userservice.addCategory(myFormData).subscribe((category:any) => {
      if(category.httpStatus == 200 && category.status == 'OK'){
        this.categoryForm.reset();
        this.getCategory();
      }
    })
  }
  getCategory(){
    this.userservice.getCategory().subscribe((category:any) => {
      if(category.httpStatus == 200 && category.status == 'OK'){
        this.categories = category.data;
      }
    })
  }

}
