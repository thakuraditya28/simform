import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup,Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
import { SocialAuthService, GoogleLoginProvider, SocialUser } from 'angularx-social-login';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm:FormGroup;
  socialUser: SocialUser;
  isLoggedin: boolean; 
  constructor(private userservice: UserService,private fb:FormBuilder,private router:Router,private socialAuthService: SocialAuthService) {
    this.loginForm = this.fb.group({
      email:['',[Validators.required,Validators.email]],
      password:['',[Validators.required,Validators.minLength(6)]],
    })
   }

  ngOnInit(): void {
    this.socialAuthService.authState.subscribe((userDada:any) => {
      if(userDada){
        this.socialLogin(userDada);
      }
    },(err) => {console.log(err)});

  }
  socialLogin(socilaresponse:any){
    this.userservice.socialLogin(socilaresponse).subscribe((user:any) => {
      this.isError = false;
      if(user.httpStatus == 200 && user.status == 'OK'){
           localStorage.setItem('accessToken',user.data[0].token)
           localStorage.setItem('userFullName',user.data[0].first_name+' '+user.data[0].last_name)
           this.router.navigate(['/dashboard']);
      }
    },(err) => {
      this.isError = true;
      this.isMsg = err.error.error[0].msg;
    });
  }
  isError:boolean = false;
  isMsg:string = '';
  login(){
    let user = this.loginForm.value;
    this.userservice.login(user).subscribe((user:any) => {
      this.isError = false;
      if(user.httpStatus == 200 && user.status == 'OK'){
           localStorage.setItem('accessToken',user.data[0].token)
           localStorage.setItem('userFullName',user.data[0].first_name+' '+user.data[0].last_name)
           this.router.navigate(['/dashboard']);
      }
    },(err) => {
      this.isError = true;
      this.isMsg = err.error.error[0].msg;
    });
  }
 
  loginWithGoogle(): void {

    this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }
  refreshGoogleToken(): void {
    this.socialAuthService.refreshAuthToken(GoogleLoginProvider.PROVIDER_ID);
  }


}
