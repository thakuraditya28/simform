import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class HeadersInterceptor implements HttpInterceptor {

  constructor() {}
header:any;
  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    if(localStorage.getItem('accessToken')){
     this.header = { 'Accept': 'application/json','token':localStorage.getItem('accessToken') };
    }else{
      this.header  =  { 'Accept': 'application/json'}
    }
    return next.handle(request.clone({ setHeaders:this.header}));
  }
}
