import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
import { SocialAuthService, GoogleLoginProvider, SocialUser } from 'angularx-social-login';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm:FormGroup;
  socialUser: SocialUser;
  isLoggedin: boolean; 
  constructor(private userservice: UserService,private fb:FormBuilder,private router:Router,private socialAuthService: SocialAuthService) {
    this.registerForm = this.fb.group({
      first_name:['',[Validators.required,Validators.minLength(3)]],
      last_name:[''],
      email:['',[Validators.required,Validators.email]],
      password:['',[Validators.required,Validators.minLength(6)]],
    })
   }

  ngOnInit(): void {
    this.socialAuthService.authState.subscribe((userDada:any) => {
      localStorage.setItem('google',JSON.stringify(userDada))
       this.socialLogin(userDada);
    },(err) => {console.log(err)});
  }

  socialLogin(socilaresponse:any){
    this.userservice.socialLogin(socilaresponse).subscribe((user:any) => {
      this.isError = false;
      if(user.httpStatus == 200 && user.status == 'OK'){
           localStorage.setItem('accessToken',user.data[0].token)
           localStorage.setItem('userFullName',user.data[0].first_name+' '+user.data[0].last_name)
           this.router.navigate(['/dashboard']);
      }
    },(err) => {
      this.isError = true;
      this.isMsg = err.error.error[0].msg;
    });
  }

  isError:boolean = false;
  isMsg:string = '';
  isSuccess:boolean = false;
  register(){
    console.log(this.registerForm.value)
    this.userservice.register(this.registerForm.value).subscribe((response:any) => {
      this.isError = false;
      if(response.httpStatus == 200 && response.status == 'OK'){
           this.isSuccess = true;
           this.isMsg = 'You have been successfully registered, now you can login'
           this.registerForm.reset();
      }
    },(err) => {
      this.isError = true;
      this.isMsg = err.error.error[0].msg;
    });
  }

  loginWithGoogle(): void {

    this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }
  refreshGoogleToken(): void {
    this.socialAuthService.refreshAuthToken(GoogleLoginProvider.PROVIDER_ID);
  }
}
