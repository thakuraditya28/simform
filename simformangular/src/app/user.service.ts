import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http:HttpClient) { }
  serviceBase = 'http://localhost:3000/';
  login(params:any) {
    return this.http.post(this.serviceBase + 'users/login', params);
  }
  register(params:any) {
    return this.http.post(this.serviceBase + 'users', params);
  }
  socialLogin(params:any) {
    return this.http.post(this.serviceBase + 'users/token', params);
  }
  addCategory(params:any){
    return this.http.post(this.serviceBase + 'category/save', params);
  }

   searchCategory(params:any){
    return this.http.post(this.serviceBase + 'category/search', params);
  }

  getCategory(){
    return this.http.get(this.serviceBase + 'category');
  }
}
