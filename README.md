# Installation

  

You need to install Node.js and then the development tools. Node.js comes with a package manager called npm for installing NodeJS applications and libraries.

  

# Simformangular

For google login you need provide the **googleClientId** in **environments/environment** file

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.6.

  

## Development server

  

Run **ng serve** for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

  

# backend node js

Our backend application server is a NodeJS application that relies upon some 3rd Party npm packages. You need to install these:

Install local dependencies (from the project root folder):

  

cd simform

npm install

  

(This will install the dependencies declared in the simform/package.json file)

  

# Configure Server

In simform directory there is common/config.json file where you can pass db username and db password, aws s3 bucket access. all images will upload to **s3 bucket** so you can need to create s3 bucket and provide **accessKeyId** and **accessSecret**

  

# database

This project runs on mongodb database so you need to run mongodb on your machine. in this database there are two collections **users** and **categories**, I have commit db dump in git repo so before start the node server, you will need to import db dump in mongodb 

  

# Start the Server

For backend server you need define a http port in **common/config.json** file.

  
 **node /bin/www**