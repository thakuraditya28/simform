/**Require mongoose dependency with validator
 * Developer : - Aditya Chauhan
 */
const mongoose = require("mongoose");
const mongoosePaginate = require('mongoose-paginate-v2');
const UserSchema = mongoose.Schema({
  first_name: {
    type: String,
    required: true
  },
  last_name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    unique : true
  },
  password: {
    type: String,
    //required: true
  },
  auth_provider:{
    type: String,
    default:'email'
  },
  access_token:{
    type: String,
    default:''
  },
  createdAt: {
    type: Date,
    default: Date.now()
  }
});
UserSchema.plugin(mongoosePaginate);
// export model category with UserSchema mongoose.model("modelName", modelSchema,'collectionName')
module.exports = mongoose.model("user", UserSchema,'users');
