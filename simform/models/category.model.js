/**Require mongoose dependency with validator
 * Developer : - Aditya Chauhan
 */
const mongoose = require("mongoose");
var uniqueValidator = require('mongoose-unique-validator');
const mongoosePaginate = require('mongoose-paginate-v2');
const categorySchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
  },
  category_image: {
    type: String,
  },
  createdAt: {
    type: Date,
    default: Date.now()
  }
});
categorySchema.plugin(uniqueValidator);
categorySchema.plugin(mongoosePaginate);
// export model category with categorySchema mongoose.model("modelName", modelSchema,'collectionName')
module.exports = mongoose.model("category", categorySchema,'categories');
