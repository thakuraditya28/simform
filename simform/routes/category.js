/**Require dependencies
 * Developer : - Aditya Chauhan
 */
 var express = require('express');
 var router = express.Router();
 var categoryController = require("../controllers/category.controller");
 const auth = require("../middleware/auth");
 const errMsg = require("../common/msg.json");
 const { check } = require('express-validator');
 const multer  = require('multer')
var upload = multer({ dest: 'uploads/' });
 const msg = errMsg.category;
 /* Define category routes */
 router.get('/', auth, categoryController.categories);
/**
 * Insert category 
 * Url /category/save
*/
router.post('/save', [
    auth,upload.single('image') ], categoryController.insertCategory);
router.post('/search',auth, categoryController.getCategory);

 
 
 module.exports = router;
 