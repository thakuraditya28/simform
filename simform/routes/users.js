/**Require dependencies
 * Developer : - Aditya Chauhan
*/
var express = require('express');
var router = express.Router();
var userController = require("../controllers/user.controller");
const { check } = require('express-validator');
const auth = require("../middleware/auth");
const user = require("../middleware/user");
const errMsg = require("../common/msg.json");
let fs = require('fs');
const msg = errMsg.user;
/* Define user routes */
router.get('/', auth, userController.users);
router.post('/token',userController.socialLogin);
router.post('/', [
  check("first_name", msg.first_name)
    .not()
    .isEmpty(),
  check("last_name", msg.last_name)
    .not()
    .isEmpty(),
    check("last_name", msg.last_name)
    .not()
    .isEmpty(),
  check("email", msg.email).isEmail(),
  check("password", msg.password).isLength({
    min: 6
  }),
  user.checkUnique,
  // user.checkUniqueUsername
], userController.insertUser);
router.post('/login', [
  check("email", msg.email).isEmail(),
  check("password", msg.password).isLength({
    min: 6
  }),
  
], userController.userLogin);

module.exports = router;
