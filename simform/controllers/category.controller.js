/**Controller dependenceies 
 * Developer : - Aditya Chauhan
*/
var categoryModel = require('../models/category.model');
const { validationResult } = require('express-validator');
const configJson = require('../common/config.json');
global.gConfig = configJson[process.env.NODE_ENV];
const GOOGLE_CLIENT_ID = global.gConfig.GOOGLE_CLIENT_ID;
const response = require('../common/response');/** Api response from ../common/response.js */
const errMsg = require("../common/msg.json");/**Error message from ../common/msg.json*/
const msg = errMsg.category;
var fs = require('fs');
const AWS = require('aws-sdk');
const s3 = new AWS.S3({
    accessKeyId: global.gConfig.aws.accessKeyId,
    secretAccessKey: global.gConfig.aws.secretAccessKey
});

/**
 * Get all categories
 */
module.exports.categories = function (req, res) {
    try {
        categoryModel.find({},null,{ sort: { createdAt: -1 } }, (err, categories) => {
            if (categories)
                return res.send(response.successResponse(200, errMsg.successMsg, categories));
            else
            console.log(err)
                return res.status(400).send(response.failedResponse(400, errMsg.badErr));
        });
    } catch (err) {
        return res.status(500).send(response.failedResponse(500, errMsg.internalErr));
    }
}
/**Get Category details */
module.exports.getCategory = function (req, res) {
    try {
        var keyword = req.body.keyword;
        var regex = new RegExp(keyword, "i");
        categoryModel.find({$or:[{ name: regex },{description:regex}]}, (err, categories) => {
            if (categories)
                return res.send(response.successResponse(200, errMsg.successMsg, categories));
            else
                return res.status(400).send(response.failedResponse(400, errMsg.badErr));
        });
    } catch (err) {
        return res.status(500).send(response.failedResponse(500, errMsg.internalErr));
    }
}
/**
 * Insert category
 */
module.exports.insertCategory = async function (req, res) {
    try {
        console.log(req.headers);
        const fileContent = fs.readFileSync(req.file.path);
            var imagename = Date.now() + ".png";
            const params = {
                Bucket: 'snapflicker',
                Key: 'category/' + imagename,
                ACL: 'public-read',
                Body: fileContent
            };
          
            s3.upload(params, function (s3Err, data) {
                if (data.Location) {
                    let category = {};
                    category.name = req.body.name
                    category.description = req.body.description
                    category.category_image = data.Location;
                    categoryModel.insertMany(category, (err, doc) => {
                        if (err) {
                            
                            return res.status(400).send(response.failedResponse(400, err.errors.name.properties.message));
                        }
                        else {
                            return res.send(response.successResponse(200, errMsg.successMsg, doc));
                        }
                    });
                } else {
                    return res.status(400).send(response.failedResponse(400, errMsg.badErr));
                }
            });
        

    } catch (err) {
        console.error(err)
        return res.status(500).send(response.failedResponse(500, errMsg.internalErr));
    }

}
