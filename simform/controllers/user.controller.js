/**Controller dependenceies 
 * Developer : - Aditya Chauhan
*/
var userModel = require('../models/user.model');

const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const salt = 10;
const configJson = require('../common/config.json');
global.gConfig = configJson[process.env.NODE_ENV];
const GOOGLE_CLIENT_ID = global.gConfig.GOOGLE_CLIENT_ID;
const { OAuth2Client } = require('google-auth-library');
const client = new OAuth2Client(GOOGLE_CLIENT_ID);
const { validationResult } = require('express-validator');
const response = require('../common/response');/** Api response from ../common/response.js */
const errMsg = require("../common/msg.json");/**Error message from ../common/msg.json*/
const msg = errMsg.user;

/**
 * Create user object for api response
 */
function createUserObject(userdata) {
    var responseData = [{
        '_id': userdata._id,
        'first_name': userdata.first_name,
        'last_name': userdata.last_name,
        'email': userdata.email,
        'auth_provider': userdata.auth_provider,
        'access_token': userdata.access_token,
        'createdAt': userdata.createdAt,
    }];
    return responseData;
}


/**
 * Get user object by jwt token _id
 */
module.exports.users = function (req, res) {
    try {
        userModel.find(req.user, (err, doc) => {
            if (doc)
                return res.send(response.successResponse(200, errMsg.successMsg, createUserObject(doc[0])));
            else
                return res.status(400).send(response.failedResponse(400, errMsg.badErr));
        });
    } catch (err) {
        return res.status(500).send(response.failedResponse(500, errMsg.internalErr));
    }
}


/**
 * User sign up function
 */
module.exports.insertUser = async function (req, res) {
    /**Validate input parameters*/
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).send(response.failedResponse(400, errors.array()));
    }

    try {
        var userData = {};
        var { first_name, last_name, password, email } = req.body;
        let hash = bcrypt.hashSync(password, salt);//Password hashing
        userData.password = hash;
        userData.email = email;
        userData.first_name = first_name;
        userData.last_name = last_name;
        userData.auth_provider = "email";
        userData.createdAt = new Date();
        userModel.insertMany(userData, (err, doc) => {
            if (err) {
                console.log(err);
                return res.status(400).send(response.failedResponse(400, errMsg.badErr));
            }
            else {
                return res.send(response.successResponse(200, errMsg.successMsg, []));
            }
        });

    } catch (err) {
        return res.status(500).send(response.failedResponse(500, errMsg.internalErr));
    }
}

function insertSocialUser(userdata) {
    return new Promise(async (resolve, reject) => {
        try {
            userdata.auth_provider = userdata.type;
            userdata.access_token = userdata.code;
            userdata.email = (userdata.email) ? userdata.email : userdata.app_user_id + "@simform.com"
            userModel.find({ email: userdata.email }, async (err, doc) => {
                if (doc.length) {
                    const payload = {
                        user: {
                            _id: doc[0]._id
                        }
                    };
                    var token = await jwt.sign(
                        payload,
                        global.gConfig.jwt_secret);
                    var responseData = createUserObject(doc[0]);
                    responseData[0].token = token;
                    resolve(responseData);
                } else {
                    userdata.createdAt = new Date();
                    userModel.insertMany(userdata, async (err, doc) => {
                        if (doc) {
                            const payload = {
                                user: {
                                    _id: doc[0]._id
                                }
                            };
                            var token = await jwt.sign(
                                payload,
                                global.gConfig.jwt_secret);
                            var responseData = createUserObject(doc[0]);
                            responseData[0].token = token;
                            resolve(responseData);
                        } else {
                            reject(errMsg.badErr);
                        }
                    });
                }
            });
        } catch (err) {
            reject(errMsg.internalErr);
        }
    });
}

/**Social Login from google */
module.exports.socialLogin = async (req, res) => {
    try {
        return client
        .verifyIdToken({
          idToken: req.body.idToken,
          audience: GOOGLE_CLIENT_ID
        })
        .then(login => {
          //if verification is ok, google returns a jwt
          var payload = login.getPayload();
          var userid = payload['sub'];
    
          //check if the jwt is issued for our client
          var audience = payload.aud;
          if (audience !== GOOGLE_CLIENT_ID) {
            return res.status(400).send(response.failedResponse(400, 'error while authenticating google user: audience mismatch: wanted'));
          }
          //promise the creation of a user
          return {
            first_name: req.body.first_name, //profile name
            last_name: req.body.last_name,
            type: 'google',
            code: req.body.idToken,
            email: payload['email']
          };
        })
        .then(user => {
            insertSocialUser(user).then((reres) => {
                return res.send(response.successResponse(200, errMsg.successMsg, reres));
            }).catch((err) => {
                return res.status(400).send(response.failedResponse(400, errMsg.badErr));
            })
        })
        .catch(err => {
            return res.status(500).send(response.failedResponse(500, errMsg.internalErr));
        });
          
    } catch (err) {
        return res.status(500).send(response.failedResponse(500, errMsg.internalErr));
    }
}

/**
 * User sign in function
 */
module.exports.userLogin = async (req, res) => {
    /**Validate input parameters */
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).send(response.failedResponse(400, errors.array()));
    }
    try {
        let userdata = await userModel.findOne({ email: req.body.email });//find user by email
        if (userdata) {
                let isLoggedIn = await bcrypt.compare(req.body.password, userdata.password);//compare password with hash password
                if (isLoggedIn) {
                    //check if password is correct
                    const payload = {
                        user: {
                            _id: userdata._id,
                        }
                    };
                    let token = await jwt.sign(
                        payload,
                        global.gConfig.jwt_secret
                    );
                    var responseData = createUserObject(userdata);
                    responseData[0].token = token;
                    return res.send(response.successResponse(200, errMsg.successMsg, responseData));
                } else {
                    return res.status(400).send(response.failedResponse(400, msg.password));
                }
            
        } else {
            return res.status(400).send(response.failedResponse(400, msg.email));
        }

    } catch (err) {
        return res.status(500).send(response.failedResponse(500, errMsg.internalErr));
    }
}

