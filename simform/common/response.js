/**httpStatus dependency 
 * Developer : - Aditya Chauhan
*/
var HttpStatus = require('http-status-codes');
/**Api successResponse function with statuscode,suceess message,Database response object */
exports.successResponse = (statusCode, message, dbResponse) => {
    const response = {
        httpStatus: statusCode,
        status: HttpStatus.getStatusText(statusCode),
        message: message,
        data: dbResponse//Database response
    }
    return response;
};


/**Api failedResponse function with status code and error message*/
exports.failedResponse = (statusCode, error) => {
    if (typeof error === 'object' || error.isArray) {
        errRes = error;
    } else {
        errRes = [{
            "value": "",
            "msg": error,
            "param": "",
            "location": "body"
        }];
    }
    const response = {
        httpStatus: statusCode,
        status: HttpStatus.getStatusText(statusCode),
        error: errRes,
    }
    return response;
}