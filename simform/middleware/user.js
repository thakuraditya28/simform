/**Middleware dependency 
 * Developer : - Aditya Chauhan
*/
var userModel = require('../models/user.model');//User model
const response = require('../common/response');/** Api response from ../common/response.js */
const errMsg = require("../common/msg.json");/**Error message from ../common/msg.json*/
const msg = errMsg.user;
/**Check email is unique */
module.exports.checkUnique = (req, res, next) => {
    userModel.find({ 'email': req.body.email }, (err, doc) => {
        if (!doc.length) {
            return next();
        } else {
            var errData = [{
                "value": req.body.email,
                "msg": msg.emailUnique,
                "param": "email",
                "location": "body"
            }];
            return res.status(400).send(response.failedResponse(400, errData));
        }
    });
}
