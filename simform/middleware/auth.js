/**Middleware dependencies 
 * Developer : - Aditya Chauhan
*/
const jwt = require("jsonwebtoken");
var userModel = require('../models/user.model');
const response = require('../common/response');/** Api response from ../common/response.js */
const errMsg = require("../common/msg.json");/**Error message from ../common/msg.json*/
const msg = errMsg.user;
/**Validate jsonwebtoken*/
module.exports = function (req, res, next) {
    const token = req.header("token");
    if (!token)
        return res.status(401).send(response.failedResponse(401, errMsg.authErr));
    try {
        const decoded = jwt.verify(token, global.gConfig.jwt_secret);
        req.user = decoded.user;//get payload user _id and append to req
        userModel.findOne(req.user, (err, doc) => {
            if (doc){
                next();
            }else{
               return res.status(401).send(response.failedResponse(401, errMsg.authErr));
            }
        });
    } catch (e) {
        return res.status(401).send(response.failedResponse(401, errMsg.authErr));

    }
};

